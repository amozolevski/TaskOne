/*Задача №1
Написать консольное приложение используя язык программирования Java для автоматической генерации
официальных документов (аналог параметрического листа MS Word).
В результате выполнения программы должно быть сгенерированно 10 документов в формате *.doc или *.docx с учетом форматирования.

Требования:
1. Для построения шаблона взять один вариант официального документа (расписка, приглашение, объявление о поощрении и тд.).
2. Список имен программа должна получить из файла *.xml
3. Имена сгенерированных файлов должны иметь формат <название документа>_<ФИО>
4. Файл *.xml должен находиться в корне приложения
 */
import org.apache.poi.xwpf.usermodel.*;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Application {

    public static void main(String args[]) throws IOException, XMLStreamException {
        XMLParser parser = new XMLParser(new FileInputStream(new File("names.xml")));
        XMLStreamReader reader = parser.getReader();

        //while find tag <name> use element text
        while (parser.doUntil(XMLEvent.START_ELEMENT, "name")){
            createDocument(reader.getElementText());
        }
    }

    private static void createDocument(String name) throws IOException{
        //blank document
        XWPFDocument document = new XWPFDocument();

        //write the document in file system
        String filePath = "target/invitations/invitation_" + name + ".docx";
        FileOutputStream out = new FileOutputStream(new File(filePath));

        //create paragraph
        XWPFParagraph paragraph = document.createParagraph();
        paragraph.setAlignment(ParagraphAlignment.CENTER);

        //set paragraphs text
        XWPFRun paragraphOneRun = paragraph.createRun();
        paragraphOneRun.setBold(true);
        paragraphOneRun.setFontSize(20);
        paragraphOneRun.setText("Let's Roll!");
        paragraphOneRun.addBreak();
        paragraphOneRun.addBreak();

        XWPFRun paragraphTwoRun = paragraph.createRun();
        paragraphTwoRun.setFontSize(18);
        paragraphTwoRun.setText("Mary is 20!");
        paragraphTwoRun.addBreak();
        paragraphTwoRun.addBreak();

        //set guest name
        XWPFRun paragraphThreeRun = paragraph.createRun();
        paragraphThreeRun.setFontSize(20);
        paragraphThreeRun.setText(name);
        paragraphThreeRun.addBreak();
        paragraphThreeRun.addBreak();

        XWPFRun paragraphFourthRun = paragraph.createRun();
        paragraphFourthRun.setFontSize(18);
        paragraphFourthRun.setText("are invited on a birthday party!");

        document.write(out);
        out.close();
        System.out.println("File invitation_" + name + ".docx created successfully...");
    }
}