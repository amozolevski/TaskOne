import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

public class XMLParser {
    private static final XMLInputFactory FACTORY = XMLInputFactory.newInstance();

    private final XMLStreamReader reader;

    public XMLParser(InputStream inputStream) throws XMLStreamException {
        reader = FACTORY.createXMLStreamReader(inputStream);
    }

    public XMLStreamReader getReader(){
        return reader;
    }

    //parse XML until can find needed value
    public boolean doUntil(int stopEvent, String value) throws XMLStreamException {
        while (reader.hasNext()){
            int event = reader.next();
            if (event == stopEvent && value.equals(reader.getLocalName())){
                return true;
            }
        }

        return false;
    }
}